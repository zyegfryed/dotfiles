#!/usr/bin/env bash

# Install oh-my-zsh
if test ! -d $HOME/.oh-my-zsh; then
    curl -sSL http://install.ohmyz.sh | sh
fi

# Install useful key bindings and fuzzy completion:
$(brew --prefix)/opt/fzf/install

# Install tmux plugin manager
mkdir -p ~/.tmux/plugins
if test ! -d $HOME/.tmux/plugins/tpm; then
    git clone https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
fi

cargo install tmux-sessionizer
