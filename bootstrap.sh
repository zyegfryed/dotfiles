#!/usr/bin/env bash

# Fail fast and fail hard.
set -eo pipefail

# initial setup on a new machine

# Install homeshick
if test ! -f $HOME/.homesick/repos/homeshick/homeshick.sh; then
    curl -fsSL https://github.com/andsens/homeshick/archive/refs/tags/v2.0.0.tar.gz \
         -o /tmp/homeshick-v2.0.0.tar.gz
    mkdir -p $HOME/.homesick/repos/homeshick
    tar zxf /tmp/homeshick-v2.0.0.tar.gz -C $HOME/.homesick/repos/homeshick --strip-components 1
    rm /tmp/homeshick-v2.0.0.tar.gz
fi
source $HOME/.homesick/repos/homeshick/homeshick.sh

# Install homeshick homes
castles=(
    dotfiles
    dotfiles-private
)
for castle in ${castles[@]}; do
    if test ! -d $HOME/.homesick/repos/${castle}; then
        homeshick clone -b https://bitbucket.org/zyegfryed/${castle}.git
    fi
done

# Initialize homeshick homes
pushd $HOME/.homesick/repos/dotfiles
git submodule init
git submodule update
popd
homeshick link dotfiles
homeshick link dotfiles-private

# Install ALL THE THINGS!!!
$HOME/.homesick/repos/dotfiles/brewed.sh
$HOME/.homesick/repos/dotfiles/utilities.sh
$HOME/.homesick/repos/dotfiles/osx-defaults.sh
