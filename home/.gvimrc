" Disable the left srcollbar.
" See: https://code.google.com/p/macvim/issues/detail?id=226
set go-=L
