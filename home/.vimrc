" No Compatibility. That just sucks
" especially annoying on redhat/windows/osx
set nocompatible
set modelines=0
set backspace=indent,eol,start

" Add .bin to path
let $PATH .= ':' . expand('~/.bin')

" Sets leader to ',' and localleader to "\"
let mapleader=","
let maplocalleader="\\"

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

Plug 'dense-analysis/ale'
Plug 'pearofducks/ansible-vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'tpope/vim-commentary'
Plug 'rizzatti/dash.vim'
Plug 'honza/dockerfile.vim'
Plug 'lambdalisue/vim-fern'
Plug 'lambdalisue/vim-fern-hijack'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'fatih/vim-go'
Plug 'junegunn/goyo.vim'
Plug 'b4b4r07/vim-hcl'
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'elzr/vim-json'
Plug 'AndrewRadev/linediff.vim'
Plug 'itchyny/lightline.vim'
Plug 'sjbach/lusty'
Plug 'LeonB/vim-nginx'
Plug 'rodjek/vim-puppet'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-sensible'
Plug 'wincent/terminus'
Plug 'jszakmeister/vim-togglecursor'
Plug 'christoomey/vim-tmux-navigator'
Plug 'mg979/vim-visual-multi'
Plug 'liuchengxu/vim-which-key'
Plug 'gcmt/wildfire.vim'

Plug 'majutsushi/tagbar', {'on': 'TagbarToggle'}

Plug 'gberenfield/cljfold.vim', {'for': 'clojure'}
Plug 'liquidz/vim-iced', {'for': 'clojure'}
Plug 'liquidz/vim-iced-kaocha', {'for': 'clojure'}
Plug 'junegunn/rainbow_parentheses.vim', {'for': 'clojure'}
Plug 'guns/vim-sexp', {'for': 'clojure'}
Plug 'tpope/vim-sexp-mappings-for-regular-people', {'for': 'clojure'}
Plug 'tpope/vim-repeat', {'for': 'clojure'}
Plug 'tpope/vim-surround', {'for': 'clojure'}
Plug 'eraserhd/parinfer-rust', {'rtp': 'target/release', 'do': 'cargo build --release'}
Plug 'itspriddle/vim-marked', {'for': 'markdown'}

Plug 'pcgen/vim-pcgen', {'for': 'pcgen'}

" LSP support
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'rhysd/vim-lsp-ale'

" Color schemes
Plug 'nordtheme/vim'

" Distraction free
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'
Plug 'vim-voom/VOoM'

" Initialize plugin system
call plug#end()

" Menus I like :-)
" This must happen before the syntax system is enabled
"aunmenu Help.
"aunmenu Window.
let no_buffers_menu=1
set mousemodel=popup

" Fix some tmux issues
if &term =~ '256color'
  set t_ut=
endif
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" Better modes. Remember where we are
set viminfo=!,'100,\"100,:20,<50,s10,h,n~/.viminfo

" Better display
set number
set relativenumber
set wrap
set textwidth=79
set formatoptions=qrn1
set colorcolumn=85

" Keep some more lines for scope
set scrolloff=5

" Turn off Vim’s crazy default regex characters and makes searches use normal regexes
nnoremap / /\v
vnoremap / /\v

" Better search
set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch

" Hide matches on <leader>space
nnoremap <leader><space> :nohlsearch<cr>

" Make the tab key match bracket pairs
nnoremap <tab> %
vnoremap <tab> %

" And be global by default
set gdefault

" Show invisible characters with the same characters that TextMate uses
set list
set listchars=tab:▸\ ,eol:¬

" Save when loosing focus
au FocusLost * :wa

" Don't bell but blink
set noerrorbells
set visualbell

" Enable syntax colors
syntax enable
set background=dark
colorscheme nord
set antialias

" ALE
" Use clj-kondo as linter for clojure code
let g:ale_linters = {'clojure': ['clj-kondo']}

" Iced
" Enable vim-iced's default key mapping
let g:iced_enable_default_key_mappings = v:true
" Position stdout buffer vertically
let g:iced#buffer#stdout#mods = 'vertical'
" Disable static analysis (use vim-ale)
let g:iced_enable_clj_kondo_analysis = v:true
" Disable auto-indentation from iced
let g:iced_enable_auto_indent = v:false

aug IcedKaochaSetting
  au!
  " Change key mappings as you like.
  au FileType clojure nmap <silent><buffer> <Leader>ktt <Plug>(iced_kaocha_test_under_cursor)
  au FileType clojure nmap <silent><buffer> <Leader>ktn <Plug>(iced_kaocha_test_ns)
  au FileType clojure nmap <silent><buffer> <Leader>ktr <Plug>(iced_kaocha_test_redo)
  au FileType clojure nmap <silent><buffer> <Leader>ktl <Plug>(iced_kaocha_test_rerun_last)
aug END

" LSP key-bindings
" Note: some bindings may collide with vim-iced ones
" Hence why we define them after
function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 10000
    autocmd! BufWritePre *.clj,*.cljc,*.edn,*.rs,*.go call execute('LspDocumentFormatSync')

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" lightline
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }

" The PC is fast enough, do syntax highlight syncing from start
autocmd BufEnter * :syntax sync fromstart

" Move Backup Files to ~/.vim/sessions
set backupdir=~/.vim/sessions
set dir=~/.vim/sessions

" Remember cursor position
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" Enable hidden buffers
set hidden

" Enable filetype plugins and indention
filetype plugin indent on

" Enable automatic title setting for terminals
set title
set titleold="Terminal"
set titlestring=%F

" activate a permanent ruler and disable Toolbar, and add line
" highlightng as well as numbers.
" And disable the sucking pydoc preview window for the omni completion
" also highlight current line and disable the blinking cursor.
set ruler
set guioptions-=T
set completeopt-=preview
set gcr=a:blinkon0
if has("gui_running")
  set cursorline
endif
set ttyfast

" customize the wildmenu
set wildmenu
set wildignore+=*.dll,*.o,*.pyc,*.bak,*.exe,*.jpg,*.jpeg,*.png,*.gif,*$py.class,*.class,*/*.dSYM/*,*.dylib
set wildmode=list:full

" configure folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
nnoremap <space> za
set foldmethod=expr
  \ foldexpr=lsp#ui#vim#folding#foldexpr()
  \ foldtext=lsp#ui#vim#folding#foldtext()

" Don't outdent hashes
inoremap # X#

" quicker window switching
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" disable arrow keys navigation
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" Always show a gutter
sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')

" autoread
set autoread

" Autoresize windows
autocmd VimResized * :wincmd =

" tab for brackets
nnoremap <tab> %
vnoremap <tab> %

" Split edit vimrc
nnoremap <leader>ev <C-w><C-s><C-l>:e $MYVIMRC<CR>

" Remove trailing whitespace on <leader>S
nnoremap <leader>S :%s/\s\+$//<cr>:let @/=''<CR>

" fuzzy matching
nmap <silent> <Leader>ff :Files<CR>
nmap <silent> <Leader>fb :Buffers<CR>

" Maps <leader>/ so we're ready to type the search keyword
nnoremap <leader>/ :Rg<CR>

" Navigate quickfix list with ease
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>

" Select the just pasted text
nnoremap <leader>v V`]

" Fern on <leader>t
nnoremap <leader>t :Fern . -drawer -toggle -reveal=%<CR>

" Hardwrap paragraphs of text <leader>q
nnoremap <leader>q gqip

" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

" Easy switching
nnoremap <leader>Tc :set ft=clojure<CR>
nnoremap <leader>Th :set ft=htmljinja<CR>
nnoremap <leader>Tp :set ft=python<CR>
nnoremap <leader>Tj :set ft=javascript<CR>
nnoremap <leader>Tr :set ft=rst<CR>

" Syntax sync
nnoremap <leader>i :syntax sync fromstart<CR>

" Tagbar plugin
nnoremap <silent> <leader>p :TagbarToggle<CR>

" Dash
nnoremap <silent> <leader>d <Plug>DashSearch

" Marked
nnoremap <leader>P :MarkedOpen<CR>

" Make the command line two lines high and change the statusline display to
" something that looks useful.
set cmdheight=2
set laststatus=2
set statusline=[%l,%v\ %P%M]\ %f\ %r%h%w\ (%{&ff})\ %{fugitive#statusline()}
set showcmd

" auoindent
set autoindent

" Tab Settings
set smarttab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" GUI Tab settings
function! GuiTabLabel()
  let label = ''
  let buflist = tabpagebuflist(v:lnum)
  if exists('t:title')
    let label .= t:title . ' '
  endif
  let label .= '[' . bufname(buflist[tabpagewinnr(v:lnum) - 1]) . ']'
  for bufnr in buflist
    if getbufvar(bufnr, '&modified')
      let label .= '+'
      break
    endif
  endfor
  return label
endfunction
set guitablabel=%{GuiTabLabel()}

" utf-8 default encoding
set encoding=utf-8

" prefer unix over windows over os9 formats
set fileformats=unix,dos,mac

" hide some files and remove stupid help
let g:netrw_list_hide='^\.,.\(pyc\|pyo\|o\)$'
map <leader>b :Explore!<CR>

" Split management
nnoremap <leader>\ <C-w>v<C-w>l
nnoremap <leader>- <C-w>s
nnoremap <leader>s :new<CR>
noremap tt :tab split<CR>

" ; is an alias for :
nnoremap ; :

" Thank you vi
nnoremap Y y$

" sudo write this
cmap W! w !sudo tee % >/dev/null

" Location bindings
noremap <leader>e :lopen<CR>
noremap <leader>] :lnext<CR>
noremap <leader>[ :lprev<CR>

" Another <esc> key
inoremap jk <Esc>

" Markdown
" enable syntax highlighting
let g:markdown_fenced_languages = ['clojure', 'html', 'python', 'vim']

" python support
" --------------
"  don't highlight exceptions and builtins. I love to override them in local
"  scopes and it sucks ass if it's highlighted then. And for exceptions I
"  don't really want to have different colors for my own exceptions ;-)
autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8
\ formatoptions=croqj softtabstop=4 textwidth=74 comments=:#\:,:#
let python_highlight_all=1
let python_highlight_exceptions=0
let python_highlight_builtins=0
let python_slow_sync=1

" ruby support
" ------------
autocmd FileType ruby setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2

" go support
" ----------
autocmd BufNewFile,BufRead *.go setlocal ft=go
autocmd FileType go setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4

" template language support (SGML / XML too)
" ------------------------------------------
" and disable that stupid html rendering (like making stuff bold etc)
autocmd FileType xml,html,htmljinja,htmldjango setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType html,htmljinja,htmldjango imap <buffer> <c-e> <Plug>SparkupExecute
autocmd FileType html,htmljinja,htmldjango imap <buffer> <c-l> <Plug>SparkupNext
autocmd FileType html setlocal commentstring=<!--\ %s\ -->
autocmd FileType htmljinja setlocal commentstring={#\ %s\ #}
let html_no_rendering=1

" CSS
" ---
autocmd FileType css setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType css setlocal commentstring=/*\ %s\ */
autocmd FileType css noremap <buffer> <leader>r :call CSSBeautify()<cr>

" Less
" ----
autocmd FileType less setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2

" Java
" ----
autocmd FileType java setlocal shiftwidth=2 tabstop=8 softtabstop=2 expandtab
autocmd FileType java setlocal commentstring=//\ %s

" rst
" ---
autocmd BufNewFile,BufRead *.txt setlocal ft=rst
autocmd FileType rst setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4
\ formatoptions+=nqt textwidth=74

" vim
" ---
autocmd FileType vim setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2

" Javascript
" ----------
autocmd FileType javascript setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal commentstring=//\ %s
autocmd FileType javascript noremap <buffer> <leader>r :call JsBeautify()<cr>
autocmd FileType javascript let b:javascript_fold = 0
let javascript_enable_domhtmlcss=1

" JSON
" ----
autocmd FileType json setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd BufNewFile,BufRead *.jsonl set filetype=json

" YAML support
" ------------
autocmd FileType yaml setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2
autocmd BufNewFile,BufRead *.sls setlocal ft=yaml

" Rainbow parentheses
" -------------------
" Activation based on file type
" au VimEnter * RainbowParentheses

" Clojure
" -------
autocmd FileType clojure setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
let g:clojure_align_multiline_strings = 1
let g:clojure_fold = 0
" let g:clojure_foldwords = "def,ns,defn,defmacro,defmethod,defschema,defprotocol,defrecord,deftest,testing"

" add fzf support
nnoremap <leader>o :FZF<cr>
nnoremap <leader>lb :Buffers<cr>
nnoremap <leader>. :Tags<cr>
let g:fzf_layout = {'down': '40%'}
set rtp+=$HOMEBREW_PREFIX/opt/fzf

" SQL
" ---
" Credits: https://github.com/darold/pgFormatter
au FileType sql setl formatprg=/usr/local/bin/pg_format\ -

" WhichKey
" --------
nnoremap <silent> <leader>      :<c-u>WhichKey ","<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  "\\"<CR>

" Focus mode
" ----------
nnoremap <leader>F :Goyo<CR>

" Fern customization
" Credits: https://github.com/lambdalisue/vim-fern/wiki/Tips#define-nerdtree-like-mappings
function! s:init_fern() abort
  " Define NERDTree like mappings
  nmap <buffer> o <Plug>(fern-action-open:edit)
  nmap <buffer> go <Plug>(fern-action-open:edit)<C-w>p
  nmap <buffer> i <Plug>(fern-action-open:split)
  nmap <buffer> gi <Plug>(fern-action-open:split)<C-w>p
  nmap <buffer> s <Plug>(fern-action-open:vsplit)
  nmap <buffer> gs <Plug>(fern-action-open:vsplit)<C-w>p
  nmap <buffer> ma <Plug>(fern-action-new-path)
  nmap <buffer> P gg

  nmap <buffer> C <Plug>(fern-action-enter)
  nmap <buffer> u <Plug>(fern-action-leave)
  nmap <buffer> r <Plug>(fern-action-reload)
  nmap <buffer> R gg<Plug>(fern-action-reload)<C-o>
  nmap <buffer> cd <Plug>(fern-action-cd)
  nmap <buffer> CD gg<Plug>(fern-action-cd)<C-o>

  nmap <buffer> I <Plug>(fern-action-hidden-toggle)

  nmap <buffer> q :<C-u>quit<CR>

  " Expand/collapse directory
  nmap <buffer><expr>
      \ <Plug>(fern-my-expand-or-collapse)
      \ fern#smart#leaf(
      \   "\<Plug>(fern-action-collapse)",
      \   "\<Plug>(fern-action-expand)",
      \   "\<Plug>(fern-action-collapse)",
      \ )

  nmap <buffer><nowait> l <Plug>(fern-my-expand-or-collapse)
endfunction

augroup fern-custom
  autocmd! *
  autocmd FileType fern call s:init_fern()
augroup END

" Activation based on file type
augroup rainbow_lisp
  autocmd!
  autocmd FileType clojure RainbowParentheses
augroup END
