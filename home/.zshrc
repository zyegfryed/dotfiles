# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if test -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="powerlevel10k/powerlevel10k"
DEFAULT_USER=zyegfryed

# igloo theme
source ~/.zsh_themes/igloo.zsh

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# better pager (syntax higlighing on man pages for example)
export PAGER=most
export BAT_PAGER="less -Rf"

# Language
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Disable auto-correct feature.
# See: http://www.yountlabs.com/journal/disable-autocorrect-in-zsh
unsetopt correct

# Add local paths
export PATH="${HOME}/bin:/opt/homebrew/bin:/usr/local/bin:/usr/local/sbin:${PATH}"

# brew completion and paths
if type brew &>/dev/null
then
    eval "$(brew shellenv)"
    # Use gnu tools instead
    export PATH="$(brew --prefix coreutils)/libexec/gnubin:${PATH}"
    # gettext path
    export PATH="$(brew --prefix gettext)/bin:${PATH}"
    # mysql path
    export PATH="$(brew --prefix mysql-client)/bin:${PATH}"
    # use brew curl as default curl
    export PATH="$(brew --prefix curl)/bin:${PATH}"
    # enable direnv hook
    type direnv &>/dev/null && eval "$(direnv hook zsh)"
    # brew completion
    FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
fi

# -- Ruby environment
export GEM_HOME=$HOME/.gem
export PATH=$GEM_HOME/bin:$PATH

if type rbenv &>/dev/null
then
    eval "$(rbenv init -)";
fi
# --//--

if type nodenv &>/dev/null
then
    eval "$(nodenv init -)"
fi
# --//--

# Homeshick (a homesick port in bash)
if test -r "$HOME/.homesick/repos/homeshick/homeshick.sh"; then
    source "$HOME/.homesick/repos/homeshick/homeshick.sh"
    FPATH="$HOME/.homesick/repos/homeshick/completions:${FPATH}"
fi

# asdf
if type asdf &> /dev/null
then
    source $(brew --prefix asdf)/libexec/asdf.sh
fi

# perl
PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

# cargo
test -s $HOME/.cargo/env && source $HOME/.cargo/env

# fzf
if type fzf &>/dev/null
then
    export FZF_BASE=$(brew --prefix fzf)
    export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git --exclude node_modules'
    export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi

# go path
export PATH="$HOME/go/bin:${PATH}"
export GOPATH=$HOME

# OPAM configuration
test -s $HOME/.opam/opam-init/init.zsh && source $HOME/.opam/opam-init/init.zsh

# iced command from vim-iced
test -d "${HOME}/.vim/plugged/vim-iced/bin" && export PATH="$PATH:${HOME}/.vim/plugged/vim-iced/bin"

# configure the SSH client to use the 1password agent for authentication.
if type op &>/dev/null
then
    export SSH_AUTH_SOCK="${HOME}/.1password/agent.sock"
fi

# oh-my-zsh
plugins=(brew docker extract fzf genpass gh git git-extras gitfast
    golang lein macos pip python tig transfer z)

source $ZSH/oh-my-zsh.sh

# Aliases
source $HOME/.aliases

# Completion
source $HOME/.completions

# Tokens
source $HOME/.tokens

# Cargo binaries
source "$HOME/.cargo/env"
