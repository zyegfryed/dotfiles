#!/usr/bin/env bash

# Fail fast and fail hard.
set -eo pipefail

# Install homebrew
if test ! $(which brew); then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

# Update homebrew
DOTFILES_PATH=$HOME/.homesick/repos/dotfiles
brew bundle install --file ${DOTFILES_PATH}/Brewfile

# Remove outdated versions from the cellar
brew cleanup

# Ensure everything is fine
brew doctor
