~/.*
====

Credits:

* Managed by [Homeshick](https://github.com/andsens/homeshick).
* Code and ideas ripped off from [Jacob Kaplan-Moss](https://github.com/jacobian/dotfiles) and [Matthew Mueller](http://lapwinglabs.com/blog/hacker-guide-to-setting-up-your-mac).

Installation:

    $ curl -L https://bitbucket.org/zyegfryed/dotfiles/raw/HEAD/bootstrap.sh | sh
